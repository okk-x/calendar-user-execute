const generateParams=(state)=>{
    let params = {};
    for (let type in state.ids) {
        params[type] = state.ids[type].map(({id}) => id);
        if(type==="org_id")params[type]=params[type][0];
    }


    state.extra.forEach(extra => {
        if (["is_performer", "is_scheduled"].includes(extra.param)&&extra.checked) params[extra.param] = extra.checked;
    });
    return params;
}

export default generateParams;
