const formatMonth=(date)=>{

    let month = date.toLocaleString('ru', {month: "long"});

    if (date.getMonth() === 7 || date.getMonth() === 2) month += "а";
    else month = month.substr(0, month.length - 1) + "я";

    return month;
}

export default formatMonth;

