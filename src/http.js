import axios from 'axios'

export default axios.create({
    baseURL: "https://dev-api.gisgkx.ru/calendar",
    headers: {
        'Content-Type': 'application/json',
    }
});
