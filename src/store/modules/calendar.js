import http from '../../http'
import generateParams from "../../helpers/generateParams";

export default {
    actions: {
        async initialCalendar(ctx, params) {
            await ctx.dispatch('fetchUsers');
            await ctx.dispatch('showByDate', params);
            ctx.commit('updateSelectedFilters');
            await ctx.dispatch('fetchTypes');
        },
        async next({commit, state, getters}) {
            if (getters.isLastPage) return;
            let page = state.list.pagination.currentPage + 1
            commit('updateLoading', true);

            await http.get(`/${state.searchType}`, {
                params: {
                    page,
                    ...state.params
                }
            }).then((response) => {
                commit("setList", {
                    type: state.searchType,
                    list: response.data.data,
                    pagination: {
                        currentPage: Number.parseInt(response.headers['x-pagination-current-page']),
                        totalPage: Number.parseInt(response.headers['x-pagination-page-count']),
                    }
                });
                commit('updateList');
                commit('updateLoading', false);
            });
        },
        async selectUser(ctx,id){
            await http.get(`${id}?expand=user,type,sector,house`).then(response=>{
                console.log(response.data.data[0]);
                ctx.commit('setUser',response.data.data[0])
            });
        },
        async search(ctx) {
            let {state} = ctx;

            localStorage.clear();
            let params = generateParams(state);

            await http.get(``, {params}).then((response) =>{
                ctx.commit("setList", {
                    list: response.data.data,
                    pagination: {
                        currentPage: Number.parseInt(response.headers['x-pagination-current-page']),
                        totalPage: Number.parseInt(response.headers['x-pagination-page-count']),
                    }
                });
            });
            localStorage.setItem('ids', JSON.stringify({...state.ids}));
            localStorage.setItem("extra", JSON.stringify(state.extra));

            ctx.commit('updateSelectedFilters');
        },
        async removeTypeAndUpdateList(ctx, type) {
            let extra = JSON.parse(localStorage.getItem("extra")) || {};
            let ids = JSON.parse(localStorage.getItem("ids")) || {};

            if (type === "extra"|| type==="all"){
                extra.map(item=>item.checked=false)
            }

            if(type==="ids" || type==="all") {
                ids = {};
            }

            else {
                ids = Object.entries(ids).reduce((init, [k, v]) => (init[k] = v, init), []);
                delete ids[type];
            }
            let params = generateParams({ids, extra});

            await http.get(``, {params}).then((response) =>{
                console.log(response);
                ctx.commit("updateTimelines",response.data.data);
            });

            localStorage.setItem('ids', JSON.stringify({...ids}));
            localStorage.setItem("extra", JSON.stringify(extra));

            ctx.commit('updateSelectedFilters');
        },
        async fetchList(ctx, {type, search}) {
            if (type !== "extra") type = type.substr(0, type.length - 3) + "s";
            if (type === ctx.state.searchType && ctx.state.beforeSearch === search) {
                ctx.commit('updateList', type);
                return;
            }
            ctx.state.beforeSearch = search;
            ctx.commit('setSearchType', type);
            ctx.commit("resetList");
            if (type === "extra") return;
            ctx.commit("setParams", type);
            ctx.commit('updateLoading', true);

            await http.get(`/${type}`, {
                params: {...ctx.state.params, filter: search.length ? search : null}
            }).then((response) => {
                ctx.commit("setList", {
                    list: response.data.data,
                    pagination: {
                        currentPage: Number.parseInt(response.headers['x-pagination-current-page']),
                        totalPage: Number.parseInt(response.headers['x-pagination-page-count']),
                    }
                });
                //ctx.commit('updateList');
                ctx.commit('updateLoading', false);
            });
        },
        async fetchTypes(ctx) {
            return http.get('/types').then(response => ctx.commit('updateTypes', response.data.data));
        },
        async fetchUsers(ctx) {
            http.get('/users').then(response => ctx.commit('initialUsers', response.data.data));
        },
        async fetchTimelines({state, commit}) {
            commit('updateTimelines', null);
            //let userIds = [];
            /*await state.users.forEach(user => {
                if (user.selected) userIds.push(user.id);
            });*/
            let ids = JSON.parse(localStorage.getItem("ids")) || {};
            let params = {};
            if (Object.keys(ids).length) {
                state.ids = Object.entries(ids).reduce((init, [k, v]) => (init[k] = v, init), []);
                state.extra = JSON.parse(localStorage.getItem("extra"));
                params = generateParams(state);
            }

            http.get("", {
                params: {
                    'date': state.date,
                    ...params,
                    //expand:'user'
                }
            }).then(response => {
                commit('updateTimelines', response.data.data)
            });
        },
        async showByDate(ctx, params) {
            ctx.commit('setDate', params);
            await ctx.dispatch('fetchTimelines');
        },
        async addType(ctx, id) {
            ctx.commit('updateTypeIds', {id, action: "ADD"});
            await ctx.dispatch('fetchTimelines');
        },
        async removeType(ctx, id) {
            ctx.commit('updateTypeIds', {id, action: "REMOVE"});
            await ctx.dispatch('fetchTimelines');
        },
        async showByTypes(ctx, params) {
            ctx.commit("setTypes", params);
            if (!params.length) ctx.commit("clearTimelines");
            else await ctx.dispatch('fetchTimelines');
        },
        async showByUsers(ctx, params) {
            ctx.commit('updateUsers', params);
            await ctx.dispatch('fetchTimelines');
        },
        async showUser(ctx, params) {
            ctx.commit('updateUserId', params);
            await ctx.dispatch('fetchTimelines');
        },
        async deleteTimeline(ctx, id) {
            await http.delete(`/${id}`)
            await ctx.dispatch('fetchTimelines');
        },
        async createRecord(ctx, {periods, description}) {
            let param = {}, params = [];
            for (let type in ctx.state.ids) {
                if (type === "user_id" || type === "type_id")
                    param[type] = ctx.state.ids[type].map(({id}) => id)[0];
                else param[type] = ctx.state.ids[type].map(({id}) => id);
            }

            let date1 = null, date2 = null, start = "", end = "";
            periods.forEach(({period, fromTime, toTime}) => {
                if (period.length === 2) {
                    date1 = period[0];
                    date2 = period[1];
                } else date1 = date2 = period[0];
                start = date1.getFullYear() + "-" + (date1.getMonth() + 1) + "-" + date1.getDate();
                end = date2.getFullYear() + "-" + (date2.getMonth() + 1) + "-" + date2.getDate();
                params.push({
                    ...param,
                    description,
                    "start": start + " " + fromTime.getHours().toString().padStart(2, "0") + ":" + fromTime.getMinutes().toString().padStart(2, "0"),
                    "end": end + " " + toTime.getHours().toString().padStart(2, "0") + ":" + toTime.getMinutes().toString().padStart(2, "0"),
                });
            })
            let res = true;
            await http.post("", params).then(response => {
                if (response.status !== 200) res = false;
            });
            await ctx.dispatch('fetchTimelines');
            return res;
        }
    },
    mutations: {
        setUser(state,user){
            state.selectedUser=user;
        },
        updateSelectedFilters(state) {
            let ids = JSON.parse(localStorage.getItem("ids")) || {};
            let extra = JSON.parse(localStorage.getItem("extra"));
            let result = [];
            if (Object.keys(ids).length && extra.length) {
                ids = Object.entries(ids).reduce((init, [k, v]) => (init[k] = v, init), []);
                let str = "";
                for (let type in ids) {
                    str = "";
                    switch (type) {
                        case 'org_id':
                            str += "Организация:";
                            break;
                        case 'type_id':
                            str += "Тип записи:";
                            break;
                        case 'sector_id':
                            str += "Участки:";
                            break;
                        case 'house_id':
                            str += "Дома:";
                            break;
                        case 'group_id':
                            str += "Группы:";
                            break;
                        default:
                            break;
                    }
                    str += " " + ids[type].map(item => item.name).join(", ");
                    result.push({str, type});
                }
            }
            state.selectedFilters = result;
        },
        clearIdsAndList(state) {
            state.ids = state.list = [];
            state.extra.forEach(extra => extra.checked = false);
            state.searchType = null;
        },
        setParams(state, type) {
            state.params = [];

            if (["sector_id", "house_id", "group_id", "position_id", "user_id"].includes(type) && state.ids["org_id"]) {
                state.params["org_id"] = state.ids["org_id"][0];
            }
            if ("house_id" === type) {
                state.params["sector_id"] = state.ids["sector_id"];
            }
            if ("user_id" === type) {
                state.params["group_id"] = state.ids["group_id"];
                state.params["position_id"] = state.ids["position_id"];
            }
        },
        updateIds(state, {name, item, payload}) {
            let id = null;
            if(item)id = parseInt(item.id);

            switch (payload) {
                case 'ADD':
                    if (state.searchType === "extra") state.extra.forEach(extra => {
                        if (extra.id === id) extra.checked = true;
                    });
                    else {
                        if (state.ids[name] === undefined) state.ids[name] = [];
                        state.ids[name].push(item);
                    }
                    break;
                case 'REMOVE':
                    if (state.searchType === "extra") state.extra.forEach(extra => {
                        if (extra.id === id) extra.checked = false;
                    });
                    else {
                        // eslint-disable-next-line no-case-declarations
                        let idx = state.ids[name].findIndex(item => item.id === id);
                        state.ids[name].splice(idx, 1);
                    }
                    break;
                case 'REMOVE_ALL':
                    if (state.searchType === "extra") state.extra.forEach(extra => extra.checked = false);
                    else state.ids[name] = [];
                    break;
            }

            if (name !== "extra" && !state.ids[name].length) delete state.ids[name];
            console.log("ids");
            console.log(state.ids);
        },
        resetList(state) {
            state.list = [];
        },
        setSearchType(state, type) {
            state.searchType = type;
        },
        updateLoading(state, loading) {
            state.loading = loading;
        },
        updateList(state) {
            if (state.list.list !== undefined) {
                state.list.list.map(item => item.checked = !!(state.ids[state.searchType] && state.ids[state.searchType].find(search => search.id === item.id)));
                return state.list.list;
            }
        },
        setList(state, {list, pagination, search}) {
            if (!search && state.list.pagination && pagination.currentPage === state.list.pagination.currentPage) return;

            if (state.list.list !== undefined && state.list.list.length && !search) {
                state.list.list.push(...list);
                state.list.pagination = pagination;
            } else {
                let type = state.searchType.substring(0, state.searchType.length - 1) + "_id";
                state.list = {
                    list: list.map(item => (item.checked = !!(state.ids[type] && state.ids[type].find(search => search.id === item.id)), item)),
                    pagination
                };
            }

        },
        clearTimelines(state) {
            state.timelines = [];
        },
        updateTypes(state, types) {
            types.forEach(type => type.selected = true);
            state.types = types;
        },
        initialUsers(state, users) {
            users.forEach(user => user.selected = true);
            state.users = users;
        },
        updateUsers(state, userId) {
            state.users.forEach(user => {
                if (user.id === userId) user.selected = !user.selected;
            });
        },
        setTypes(state, ids) {
            state.typeIds = ids;
            state.types.forEach(type => {
                type.selected = ids.includes(type.id);
            });
        },
        updateTypeIds(state, {id, action}) {
            const ids = state.typeIds;
            const type = state.types.find(type => type.id === id);
            switch (action) {
                case 'ADD':
                    if (!ids.includes(id))
                        ids.push(id);
                    type.selected = true;
                    break;
                case 'REMOVE':
                    // eslint-disable-next-line no-case-declarations
                    let index = ids.indexOf(id);
                    if (index >= 0)
                        ids.splice(index, 1);
                    type.selected = false;
                    break;
            }
        },
        setDate(state, date) {
            state.date = date.toLocaleString('ru', {year: "numeric"}) + "-" + date.toLocaleString('ru', {month: "numeric"}).padStart(2, '0');
        },
        updateTimelines(state, timelines) {
            state.timelines = timelines;
        }
    },
    state: {
        users: [],
        types: [],
        timelines: null,
        date: null,
        userIds: [],
        typeIds: [],
        params: {},
        ids: [],
        list: [],
        beforeSearch: "",
        extra: [
            {
                id: 1,
                name: "Отображать пустые строки",
                param: "is_empty",
                checked: false
            },
            {
                id: 2,
                name: "Отображать только работающих по графику",
                param: "is_scheduled",
                checked: false
            },
            {
                id: 3,
                name: "Отображать только исполнителей",
                param: "is_performer",
                checked: false
            }

        ],
        searchType: null,
        loading: false,
        selectedFilters: [],
        selectedUser:{},
    },
    getters: {
        selectedUser(state){
            console.log(state.ids);
          return state.selectedUser;
        },
        isLastPage(state) {
            let {currentPage, totalPage} = state.list.pagination;
            return currentPage === totalPage;
        },
        loading(state) {
            return state.loading;
        },
        getList(state) {
            if (state.searchType === "extra") return state.extra;
            return state.list.list ? state.list.list : [];
        },
        allUsers(state) {
            return state.users;
        },
        allTypes(state) {
            return state.types;
        },
        allSelectedFilters(state) {
            return state.selectedFilters;
        },
        allUsersWithTimelines(state) {
            const {users, timelines} = state;
            if (!users || !timelines) return null;
            const ret = users.map(({id, name, selected, url}) => {
                return {
                    id,
                    name,
                    selected,
                    url
                }
            });
            let user = null;

            timelines.map(timeline => {
                if ((user = ret.find(item => item.id === timeline['user_id'])) !== undefined) {
                    if (user.timelines === undefined) user.timelines = [];
                    user.timelines.push({
                        "id": timeline["id"],
                        'type_id': timeline['type_id'],
                        'description': timeline['description'],
                        'start': timeline['start'],
                        'end': timeline['end']
                    });
                }
            });

            ret.forEach(item => item.timelines !== undefined && item.timelines.sort((a, b) => a.start - b.start));

            console.log(ret);

            return ret;
        }
    }
}
